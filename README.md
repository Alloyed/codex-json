An unofficial transcribing of the card text for Codex: Card-Time Strategy, in
json format.  The format is mostly self-explanatory.

Each .json file represents a single card, except for the top-level
files. File/directory structure is undefined and subject to change otherwise.

Unit cards will always have the following fields:

```
name
type : "unit"
class
spec
tech
cost
attack
health
```

Spells will have these fields:

```
name
type : "spell"
class
spec
cost
effectText
```

and Heros will have these:

```
name
type : "hero"
class
spec
levels
```

For cards with effects, a basic attempt at classification is stored in
the "effects" field. This field is an array where each element is a
seperate effect. There are two effect types:

* Shared effects are effects that are shared by multiple units and have
  a single name. examples include "haste" and "resist 1".

* Effect traits are parts of a unique effect that are shared. These can
  be chained with other effect traits using the "|" character. For example,
  the string to represent a unit ability that taps its unit and targets another
  would be "exhaust | target".


Heroes can change their stats based on on their level. This is modeled
using a hashtable, like so
```
levels : {
  "1" : { ...level 1 stats },
  "2-3" : { ...level 2 and 3 stats},
}
```
These are denoted using inclusive "M-N" ranges. In the above example,
"2-4" represents levels 2, 3, and 4. "M" is equivalent to "M-M".

Notes:

* Card art/flavor text is not provided, this is a mechanics resource. 

* No unique card ids are provided, because Codex does not have card ids.
  Use the "name" field if necessary.

* Only Bashing and Finesse are provided because I'm a PnP scrub.

Codex is (c) Sirlin Games
